import requests
from flask import Flask, render_template, request, redirect, url_for, flash
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['DEBUG'] = True
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///ciudades.db'
app.config['SECRET_KEY'] = 'EstoEsSecreto'

db = SQLAlchemy(app)


class Ciudad(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)


def extraer_datos(ciudad):
    url = f'http://api.openweathermap.org/data/2.5/weather?q={ ciudad }&units=metric&appid=c13484871c411be98bd62cdb562ede42'
    r = requests.get(url).json()
    return r


@app.route('/')
def index_get():
    ciudades = Ciudad.query.all()
    datos_tiempo = []
    for ciudad in ciudades:
        r = extraer_datos(ciudad.name)
        print(r)
        tiempo = {
            'ciudad': ciudad.name,
            'termica': r['main']['feels_like'],
            'temp_actual' : r['main']['temp'],
            'temperatura_min': r['main']['temp_min'],
            'temperatura_max': r['main']['temp_max'],
            'presion_atmos': r['main']['pressure'],
            'descripcion': r['weather'][0]['description'],
            'icono': r['weather'][0]['icon'],
        }
        datos_tiempo.append(tiempo)
    return render_template('index.html', datos_tiempo=datos_tiempo)


@app.route('/', methods=['POST'])
def index_post():
    error_ = ''
    ciudad_nueva = request.form.get('ciudad')
    if ciudad_nueva:
        ya_existe = Ciudad.query.filter_by(name=ciudad_nueva).first()
        if not ya_existe:
            verificarlo = extraer_datos(ciudad_nueva)
            if verificarlo['cod'] == 200:
                ciudad_nueva_objeto = Ciudad(name=ciudad_nueva)
                db.session.add(ciudad_nueva_objeto)
                db.session.commit()
            else:
                error_ = "La ciudad no existe"
        else:
            error_ = "Ya existe esa ciudad"

    if error_:
        flash(error_, 'error')
    else:
        flash("Ciudad agregada con exito")

    return redirect(url_for('index_get'))


@app.route('/eliminar/<nombre>')
def eliminar_ciudad(nombre):
    ciudad = Ciudad.query.filter_by(name=nombre).first()
    db.session.delete(ciudad)
    db.session.commit()
    flash(f'Se elimino {ciudad.name}', 'Hecho')
    return redirect(url_for('index_get'))


if __name__ == '__main__':
    app.run()
    #host='0.0.0.0', port=8080, debug=True